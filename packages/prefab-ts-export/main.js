'use strict';

/* // "i18n:MAIN_MENU.package.title/ui-creator/create-ui-template": {
//     "message": "ui-creator:create-ui-template"
// }, */

const dontSelectCorrectAssetMsg = {
    type: 'warning',
    buttons: ['OK'],
    title: 'warning',
    message: 'Please select a UI prefab!',
    defaultId: 0,
    noLink: true
};

const nodeTree = require('./core/node-tree');
// const uiTemplate = require('./core/ui-template');

module.exports = {
    load() {
        // execute when package loaded
        nodeTree.init();
    },

    unload() {
        // execute when package unloaded
    },

    // register your ipc messages here
    messages: {
        'export'() {
            let currentSelection = Editor.Selection.curSelection('asset');
            if (currentSelection.length <= 0) {
                Editor.Dialog.messageBox(dontSelectCorrectAssetMsg);
                return;
            }
            let selectionUUid = currentSelection[0];
            let assetInfo = Editor.assetdb.assetInfoByUuid(selectionUUid);

            let assetType = assetInfo.type;
            if (assetType === 'prefab') {
                nodeTree.dealPrefab(assetInfo);
            } else {
                Editor.Dialog.messageBox(dontSelectCorrectAssetMsg);
            }
        }
    }
};