'use strict'

module.exports.port = {
    exportPath: '', // 为空字符串时，脚本默认导出在同级文件夹
    exportFlag: '$', // 导出标识 即以指定标识开头命名的节点会被导出到脚本，如 $node
    useAutoBind: true, // 是否使用自动绑定，为true时使用模板文件tsTempAutoBind.ts，否则是tsTemp.ts
}

// 需要导出为对应组件时 节点命名为 $btn_testBtn则会导出该节点下的cc.Button
// 普通节点直接为 $nodeName
module.exports.compMap = {
    'btn_' : 'cc.Button',
    'lab_': 'cc.Label', 
    'edit_': 'cc.EditBox',
    'scro_': 'cc.Scroll',
    'labR_': 'cc.RichText',
    'spine_': 'cc.Spine',
    'sp_': 'cc.Sprite',
    'bar_': 'cc.ProgressBar',
}