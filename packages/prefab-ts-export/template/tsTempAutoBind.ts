/**
 * export by auto
 * author:
 * Date: _DATE_
 * Desc:
 */

const {ccclass, property} = cc._decorator;
const {metadata} = vv._decorator;

@ccclass
export default class _CLASS_NAME_ extends vv.BaseView {
    
    //--Auto export attr, Don't change--
    //--Auto export attr, Don't change end--
    protected _autoBind: boolean = true;

    /** 界面初始化 在 onLoad 和 onEnable执行之后  start执行之前 */
    protected init(arg) {
        // TODO
    }

    /** 
     * 一些UI初始化 
     */
    protected _initUI() {
        // TODO
    }

    /** 
     * 初始化事件 
     */
    protected _initEvent() {
        // TODO
    }

    /** 
     * 界面关闭 
     */
    protected onClose() {
        // TODO
    }
}
