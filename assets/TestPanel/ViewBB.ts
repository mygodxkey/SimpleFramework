/**
 * export by auto
 * author:
 * Date: 2021/5/12  00:01:06
 * Desc:
 */

import { ViewZOrder } from "../Framework/ViewZOrder";

const {ccclass, property} = cc._decorator;
const {metadata} = vv._decorator;

@ccclass
export default class ViewBB extends vv.BaseView {
    
    //--Auto export attr, Don't change--
 	@metadata("cc.Label")
	private _lab_log: cc.Label = null;
	//--Auto export attr, Don't change end--
    protected _autoBind: boolean = true;

    public LayerOrder = ViewZOrder.PopView; // 改变层级

    /** 
     * 一些UI初始化 
     */
     protected _initUI() {
        this._lab_log.string = ''
    }

    Test_Evt(arg1, arg2) {
        this._lab_log.string = `Viewb 接收到事件 Test_Evt\n事件参数 arg1: ${arg1} arg2: ${arg2}`;
    }
}
