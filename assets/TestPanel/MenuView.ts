/**
 * export by auto
 * author:
 * Date: 2021/5/11  23:55:18
 * Desc:
 */

import { ViewZOrder } from "../Framework/ViewZOrder";

const {ccclass, property} = cc._decorator;
const {metadata} = vv._decorator;

@ccclass
export default class MenuView extends vv.BaseView {
    
    //--Auto export attr, Don't change--
 	@metadata("cc.Button")
	private _btn_viewA: cc.Button = null;
	@metadata("cc.Button")
	private _btn_closeA: cc.Button = null;
	@metadata("cc.Button")
	private _btn_viewB: cc.Button = null;
	@metadata("cc.Button")
	private _btn_closeB: cc.Button = null;
	@metadata("cc.Button")
	private _btn_viewAA: cc.Button = null;
	@metadata("cc.Button")
	private _btn_closeAA: cc.Button = null;
	@metadata("cc.Button")
	private _btn_viewBB: cc.Button = null;
	@metadata("cc.Button")
	private _btn_closeBB: cc.Button = null;
	@metadata("cc.Button")
	private _btn_evtA: cc.Button = null;
	//--Auto export attr, Don't change end--
    protected _autoBind: boolean = true;

	public LayerOrder = ViewZOrder.TopView; // 改变层级

    clickBtnViewA() {
		console.log('通过方法 open 打开ViewA')
		vv.viewMgr.open('ViewA', 'ViewA', 2);
	}

	clickBtnViewB() {
		console.log('通过方法 openSync 打开ViewA')
		vv.viewMgr.openSync('ViewB', 'ViewB', 22);
	}

	clickBtnCloseA() {
		vv.viewMgr.close('ViewA');
	}

	clickBtnCloseB() {
		vv.viewMgr.close('ViewB');
	}

	clickBtnViewAA() {
		vv.viewMgr.open('ViewAA', 'ViewAA', 2);
	}

	clickBtnViewBB() {
		vv.viewMgr.openSync('ViewBB', 'ViewBB', 22);
	}

	clickBtnCloseAA() {
		vv.viewMgr.close('ViewAA');
	}

	clickBtnCloseBB() {
		vv.viewMgr.close('ViewBB');
	}

	clickBtnEvtA() {
		vv.evtMgr.emit('Test_Evt', 1, 2);
	}
}
