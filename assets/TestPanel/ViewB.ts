/**
 * export by auto
 * author:
 * Date: 2021/5/12  00:01:05
 * Desc:
 */

const {ccclass, property} = cc._decorator;
const {metadata} = vv._decorator;

@ccclass
export default class ViewB extends vv.BaseView {
    
    //--Auto export attr, Don't change--
 	@metadata("cc.Label")
	private _lab_log: cc.Label = null;
	//--Auto export attr, Don't change end--
    protected _autoBind: boolean = true;

    /** 界面初始化 在 onLoad 和 onEnable执行之后  start执行之前 */
    protected init(arg1, arg2) {
        console.log('ViewB 初始化 接收到传入参数', arg1, arg2)
    }

    /** 
     * 一些UI初始化 
     */
    protected _initUI() {
        this._lab_log.string = '';
    }

    Test_Evt(arg1, arg2) {
        this._lab_log.string = `ViewB 接收到事件 Test_Evt\n事件参数 arg1: ${arg1} arg2: ${arg2}`;
    }
}
