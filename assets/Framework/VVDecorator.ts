
let metaMap = {};

export function Metadata(description: string, clsName) {
    return (target, name) => {
        if (CC_EDITOR) return;
        let regist = () =>{
            let clsName = target["__classname__"] || "default";
            if (!CC_BUILD) {
                clsName = target.constructor.name
            }
            if (!metaMap[clsName]) {
                metaMap[clsName] = {};
            }
            let map = metaMap[clsName];
            map[name] = description;
        }
        if (CC_BUILD) {
            setTimeout(regist, 1)
        }
        else {
            regist();
        }
    }
}

/** 
 * 获取标记属性 
 * @param {string} className 类名
 * @param {string} attrKey 属性名
 */
export function getMatedata(className: string, attrKey: string) {
    // console.log('----->', metaMap[className])
    if (!metaMap[className]) {
        return null;
    }
    return metaMap[className][attrKey] || null;
}
