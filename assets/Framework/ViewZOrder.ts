
/** view 层级 */
export const ViewZOrder = {
    MainView: 0,
    SubView: 200,
    PopView: 500,
    TopView: 900,
    TipsView: 999,
}