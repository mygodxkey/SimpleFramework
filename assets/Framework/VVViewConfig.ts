
/** 界面配置 */
const ViewConfig = {
    'MenuView': {BundlePath: 'TestPanel', PrefabPath: 'MenuView', preventTouch: false},
    'ViewA': {BundlePath: 'TestPanel', PrefabPath: 'ViewA', preventTouch: false},
    'ViewB': {BundlePath: 'TestPanel', PrefabPath: 'ViewB', preventTouch: false},
    'ViewAA': {BundlePath: 'TestPanel', PrefabPath: 'ViewAA', preventTouch: false},
    'ViewBB': {BundlePath: 'TestPanel', PrefabPath: 'ViewBB', preventTouch: false},
}
export default ViewConfig

export class VConf {
    BundlePath: string;
    PrefabPath: string;
    /** 在界面开启前屏蔽点击 */
    preventTouch: boolean = false;
}