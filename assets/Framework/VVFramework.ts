declare let GameGlobal: any;

let envGloable = typeof window === 'undefined'? GameGlobal: window
envGloable['vv'] = {};

import BaseComp from './View/VVBaseComp';
(envGloable['vv'] as any).BaseComp = BaseComp;

import BaseView from './View/VVBaseView';
(envGloable['vv'] as any).BaseView = BaseView;

import ViewManager from './View/VVViewManager';
(envGloable['vv'] as any).viewMgr = ViewManager;

import EventManager from './VVEventManager';
(envGloable['vv'] as any).evtMgr = EventManager;

import ResLoader from './VVResLoader';
(envGloable['vv'] as any).resLoader = ResLoader;

import {Metadata, getMatedata} from './VVDecorator';
(envGloable['vv'] as any)._decorator = (envGloable['vv'] as any)._decorator || {};
let self_decorator = (envGloable['vv'] as any)._decorator
self_decorator.metadata = Metadata;
self_decorator.getMatedata = getMatedata;