`use strict`

window.vv = window.vv || {};
let metaMap = {};

vv.metadata = function (description) {
    return (target, name) => {
        if (CC_EDITOR) return;
        let regist = () =>{
            let clsName = target["__classname__"] || "default";
            if (cc.sys.platform === cc.sys.DESKTOP_BROWSER) {
                clsName = target.constructor.name
            }
            if (!metaMap[clsName]) {
                metaMap[clsName] = {};
            }
            let map = metaMap[clsName];
            map[name] = description;
        }
        if (cc.sys.platform !== cc.sys.DESKTOP_BROWSER) {
            setTimeout(regist, 1)
        }
        else {
            regist();
        }
    }
}

vv.getMatedata = function (className, attrKey) {
    // console.log('----->', metaMap[className])
    if (!metaMap[className]) {
        return null;
    }
    return metaMap[className][attrKey] || null;
}
