`use strict`

let BaseComp = cc.Class({
    extends: cc.Component,
    
    ctor() {
        this._autoEvents = [];
    },

    _autoOnListen() {
        let keys = Object.keys(this['__proto__']);
        keys.forEach(k => {
            if (this._autoEvents.indexOf(k) === -1 && k.match(/.+_.+/) && typeof(this[k]) === 'function') {
                this._autoEvents.push(k);
                EventManager.on(k, this[k], this);
            }
        })
    },

    _autoOffListen() {
        this._autoEvents.forEach(k => {
            EventManager.off(k, this[k], this);
        })
        this._autoEvents = [];
    }
});

window.vv = window.vv || {};
vv.BaseComp = BaseComp;
